import time


class MinimaxAB:
    def __init__(self, color, opponent_color, start_time, time_limit):
        self.color = color
        self.opponent_color = opponent_color
        self.start_time = start_time
        self.time_limit = time_limit

    def alpha_beta(self, node, depth, is_maximizing_player, alpha, beta):
        if len(node.get_children()) == 0:
            if node.board.win(self.color):
                node.set_utility(float('inf'))
                return None, node.get_utility()
            if node.board.win(self.opponent_color):
                node.set_utility(-float('inf'))
                return None, node.get_utility()
            node.set_eval_func(self.color)
            return None, node.get_utility()

        if is_maximizing_player:
            val = -float('inf')
            for child_node in node.get_children():
                node_ab, val = self.alpha_beta(child_node, depth + 1, False, alpha, beta)
                if val >= beta:
                    return child_node, val
                alpha = max(alpha, val)
            return child_node, val
        else:
            val = float('inf')
            for child_node in node.get_children():
                node_ab, val = self.alpha_beta(child_node, depth + 1, True, alpha, beta)
                if val <= alpha:
                    return child_node, val
                beta = min(beta, val)
            return child_node, val

