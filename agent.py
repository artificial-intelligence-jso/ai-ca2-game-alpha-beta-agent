import functools
import time

from alphaBeta import AlphaBeta
from minimaxAB import MinimaxAB
from treeAB import TreeAB


def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


class Agent:
    def __init__(self, color, opponent_color, time_limit=None):
        self.color = color
        self.opponent_color = opponent_color
        self.height = 3
        self.time_limit = time_limit

    @timer
    def move(self, board):
        start_time = time.time()
        # game_tree = TreeAB(board, self.color, self.opponent_color, self.height)
        # minimaxAB = MinimaxAB(self.color, self.opponent_color, start_time, self.time_limit)
        # node_ab, val = minimaxAB.alpha_beta(game_tree.root, 0, True, -float('inf'), float('inf'))
        alpha_beta = AlphaBeta(board, self.color, self.opponent_color, start_time, self.time_limit)
        node_ab, val = alpha_beta.find_best_move()
        # print(node_ab.board.board)
        return node_ab.get_from_cell(), node_ab.get_to_cell()
