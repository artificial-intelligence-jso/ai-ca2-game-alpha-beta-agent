import copy
from nodeAB import NodeAB


class TreeAB:
    def __init__(self, board, color, opponent_color, height):
        self.height = height
        self.board = board
        self.nodes = [[] for i in range(self.height+1)]
        self.root = self.make_node(0, board, None)
        self.build_tree(color, opponent_color)

    def make_node(self, height, board, parent, from_cell=None, to_here_cell=None):
        node = NodeAB(board, parent, from_cell, to_here_cell)
        self.nodes[height].append(node)
        return node

    def build_tree(self, color, opponent_color):
        max_nodes = [self.root]
        min_nodes = []
        max_turn = True
        for i in range(self.height):
            if max_turn:
                min_nodes = self.make_minimax_children_for(max_nodes, color, i+1)
                max_turn = False
            else:
                max_nodes = self.make_minimax_children_for(min_nodes, opponent_color, i+1)
                max_turn = True

    def make_minimax_children_for(self, nodes, color, height):
        result_nodes = []
        for node in nodes:
            pieces_from_cell, pieces_to_cell = node.board.getPiecesPossibleLocations(color)
            for i in range(len(pieces_to_cell)):
                for j in range(len(pieces_to_cell[i])):
                    new_board = copy.deepcopy(node.board)
                    new_board.changePieceLocation(color, pieces_from_cell[i], pieces_to_cell[i][j])

                    child_node = self.make_node(height, new_board, node, pieces_from_cell[i], pieces_to_cell[i][j])
                    result_nodes.append(child_node)
                    node.add_child(child_node)

        return result_nodes

    def print_nodes(self):
        for i, level in enumerate(self.nodes):
            print("################# level: " + str(i) + " ################# \n")
            for node in level:
                node.print_node()
