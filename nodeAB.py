class NodeAB:
    def __init__(self, board, parent, from_cell, to_here_cell):
        self.heuristic_values = {
            "pawn_count": 6,
            "furthest_pawn": 8,
            "movable_pawn": 4,
            "unhindered_pawn": 10
        }
        self.parent = parent
        self.children = []
        self.utility = 0
        self.board = board
        self.from_cell = from_cell
        self.to_here_cell = to_here_cell

    def add_child(self, child_node):
        self.children.append(child_node)

    def set_utility(self, utility):
        self.utility = utility

    def get_utility(self):
        return self.utility

    def set_eval_func(self, color):
        pawn_count_const = self.heuristic_values.get('pawn_count')
        furthest_pawn_const = self.heuristic_values.get('furthest_pawn')
        movable_pawn_const = self.heuristic_values.get("movable_pawn")
        unhindered_pawn_const = self.heuristic_values.get("unhindered_pawn")
        evaluation = pawn_count_const * pawn_count_eval(self.board, color) + \
                     furthest_pawn_const * furthest_pawn_eval(self.board, color) + \
                     movable_pawn_const * movable_pawn_eval(self.board, color) + \
                     unhindered_pawn_const * unhindered_pawn_eval(self.board, color)
        self.utility = evaluation

    def get_parent(self):
        return self.parent

    def get_children(self):
        return self.children

    def get_from_cell(self):
        return self.from_cell

    def get_to_cell(self):
        return self.to_here_cell

    def print_node(self):
        print(self.board.board)


def pawn_count_eval(board, color):
    return board.getNumberOfArmy(color)


def furthest_pawn_eval(board, color):
    if color == "W":
        for num, row in enumerate(reversed(board.board)):
            if "W" in row:
                return board.n_rows - num
    if color == "B":
        for num, row in enumerate(board.board):
            if "B" in row:
                return board.n_rows - num


def movable_pawn_eval(board, color):
    pieces_from_cell, pieces_to_cell = board.getPiecesPossibleLocations(color)
    return len(pieces_from_cell)


def unhindered_pawn_eval(board, color):
    opponent_color = "W" if color == "B" else "B"
    pieces_from_cell_me, pieces_to_cell_me = board.getPiecesPossibleLocations(color)
    pieces_from_cell_opponent, pieces_to_cell_opponent = board.getPiecesPossibleLocations(opponent_color)
    all_pieces = pieces_from_cell_me + pieces_from_cell_opponent
    unhindered = 0
    for piece in pieces_from_cell_me:
        is_unhindered = 1
        for p in all_pieces:
            if piece[1] == p[1] and piece[0] != p[0]:
                is_unhindered = 0
        unhindered += is_unhindered
    return unhindered
