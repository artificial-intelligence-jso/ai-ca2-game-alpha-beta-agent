import copy
import time

from nodeAB import NodeAB


class AlphaBeta:
    def __init__(self, board, color, opponent_color, start_time, time_limit):
        self.board = board
        self.color = color
        self.opponent_color = opponent_color
        self.start_time = start_time
        self.time_limit = time_limit
        self.root = self.make_node(board, None)

    @staticmethod
    def make_node(board, parent, from_cell=None, to_here_cell=None):
        node = NodeAB(board, parent, from_cell, to_here_cell)
        return node

    def minimax_ab(self, node, depth, is_maximizing, alpha, beta):
        inf = float('inf')
        if len(node.get_children()) == 0:
            if node.board.win(self.color):
                node.set_utility(inf)
                return node, node.get_utility(), False
            if node.board.win(self.opponent_color):
                node.set_utility(-inf)
                return node, node.get_utility(), False

        if depth == 0:
            node.set_eval_func(self.color)
            return node, node.get_utility(), False

        if is_maximizing:
            if len(node.get_children()) == 0:
                pieces_from_cell, pieces_to_cell = node.board.getPiecesPossibleLocations(self.color)
                val = -inf
                best = 0
                best_node = node
                for i in range(len(pieces_to_cell)):
                    for j in range(len(pieces_to_cell[i])):
                        new_board = copy.deepcopy(node.board)
                        new_board.changePieceLocation(self.color, pieces_from_cell[i], pieces_to_cell[i][j])
                        child_node = self.make_node(new_board, node, pieces_from_cell[i], pieces_to_cell[i][j])

                        node_ab, val, done = self.minimax_ab(child_node, depth - 1, False, alpha, beta)
                        if val >= best:
                            best = val
                            best_node = child_node
                        alpha = max(alpha, best)
                        if (time.time() - self.start_time) * 1000 >= self.time_limit * 1000:
                            return best_node, best, True
                        if alpha >= beta:
                            return best_node, best, False
            else:
                val = -inf
                best = 0
                best_node = node
                for child_node in node.get_children():
                    node_ab, val, done = self.minimax_ab(child_node, depth - 1, False, alpha, beta)
                    if val >= best:
                        best = val
                        best_node = child_node
                    alpha = max(alpha, best)
                    if (time.time() - self.start_time) * 1000 >= self.time_limit * 1000:
                        return best_node, best, True
                    if alpha >= beta:
                        return best_node, best, False
            return best_node, best, False
        else:
            if len(node.get_children()) == 0:
                pieces_from_cell, pieces_to_cell = node.board.getPiecesPossibleLocations(self.opponent_color)
                val = inf
                best = 0
                best_node = node
                for i in range(len(pieces_to_cell)):
                    for j in range(len(pieces_to_cell[i])):
                        new_board = copy.deepcopy(node.board)
                        new_board.changePieceLocation(self.opponent_color, pieces_from_cell[i], pieces_to_cell[i][j])
                        child_node = self.make_node(new_board, node, pieces_from_cell[i], pieces_to_cell[i][j])
                        node_ab, val, done = self.minimax_ab(child_node, depth - 1, True, alpha, beta)
                        if val <= best:
                            best_node = child_node
                            best = val
                        beta = min(beta, best)
                        if (time.time() - self.start_time) * 1000 >= self.time_limit * 1000:
                            return best_node, best, True
                        if beta <= alpha:
                            return best_node, best, False
            else:
                val = inf
                best = 0
                best_node = node
                for child_node in node.get_children():
                    node_ab, val, done = self.minimax_ab(child_node, depth - 1, True, alpha, beta)
                    if val <= best:
                        best_node = child_node
                        best = val
                    beta = min(beta, best)
                    if (time.time() - self.start_time) * 1000 >= self.time_limit * 1000:
                        return best_node, best, True
                    if beta <= alpha:
                        return best_node, best, False
            return best_node, best, False

    def find_best_move(self):
        inf = float('inf')
        best_move = self.root
        best_val = 0
        i = 0
        while True:
            node_ab, val, done = self.minimax_ab(self.root, i, True, -inf, inf)
            i += 1
            if val >= best_val:
                best_move = node_ab
                best_val = val
            if done:
                return best_move, best_val
