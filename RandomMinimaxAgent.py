from minimax import Minimax
from tree import Tree
import functools
import time


def timer(func):
    """Print the runtime of the decorated function"""
    @functools.wraps(func)
    def wrapper_timer(*args, **kwargs):
        start_time = time.perf_counter()    # 1
        value = func(*args, **kwargs)
        end_time = time.perf_counter()      # 2
        run_time = end_time - start_time    # 3
        print(f"Finished {func.__name__!r} in {run_time:.4f} secs")
        return value
    return wrapper_timer


class RandomMinimaxAgent:
    def __init__(self, myColor, opponentColor):
        self.myColor = myColor
        self.opponentColor = opponentColor
        self.height = 3

    # @timer
    def move(self, board):
        gameTree = Tree(board, self.myColor, self.opponentColor, self.height)
        from_cell, to_cell = Minimax.calNextMove(gameTree, self.height)
        # print(from_cell, to_cell)
        return from_cell, to_cell
